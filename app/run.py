from app import Application


app = Application()


def main():
    app.run()


if __name__ == '__main__':

    main()
