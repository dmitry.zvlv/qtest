"""
Модуль работы с telegram
Предоставляет возможность использования методов aiogram в качестве декораторов методов своего класса
"""
import logging
import sys
from functools import wraps, partial

from aiogram import Bot, Dispatcher, types, filters
from aiogram.enums import ParseMode
from aiogram.webhook.aiohttp_server import SimpleRequestHandler, setup_application


async def on_startup(bot: Bot, options) -> None:
    await bot.delete_webhook(drop_pending_updates=True)
    # If you have a self-signed SSL certificate, then you will need to send a public
    # certificate to Telegram
    await bot.set_webhook(f"{options.base_webhook_url}{options.webhook_url}")
    # await bot.set_webhook(f"https://totmascot.es/webhook")


def setup(application, webapp) -> None:
    dp = Dispatcher()
    options = application.opts.telegram

    for name in [func for func in dir(application) if callable(getattr(application, func))]:
        func = getattr(application, name)
        if hasattr(func, 'tgm'):
            if func.tgm['filter'] is None:
                dp.message()(func)
            else:
                dp.message(func.tgm['filter'])(func)

    # Register startup hook to initialize webhook
    dp.startup.register(partial(on_startup, options=options))

    # Initialize Bot instance with a default parse mode which will be passed to all API calls
    bot = Bot(options.token, parse_mode=ParseMode.HTML)

    # Create an instance of request handler,
    # aiogram has few implementations for different cases of usage
    # In this example we use SimpleRequestHandler which is designed to handle simple cases
    webhook_requests_handler = SimpleRequestHandler(
        dispatcher=dp,
        bot=bot)
    # Register webhook handler on application
    webhook_requests_handler.register(webapp, path=options.webhook_url)

    # Mount dispatcher startup and shutdown hooks to aiohttp application
    setup_application(webapp, dp, bot=bot)


def handler(_filter: filters = None):
    def wrapper(func):
        @wraps(func)
        async def inner(slf, message: types.Message, *args, **kwargs):
            try:
                return await func(slf, message, *args, **kwargs)
            except Exception:
                logging.exception('Необработанная ошибка')

        inner.tgm = dict(filter=_filter)
        return inner

    return wrapper

