"""
Модуль приложения qTest
"""

import asyncio
import decimal
import re
import logging
import random
from concurrent.futures import ProcessPoolExecutor
from io import BytesIO
import os

from aiogram.types import Message
from aiogram.types import BufferedInputFile
from aiogram.utils.markdown import hbold
from aiogram.filters import CommandStart
from aiopg.sa import create_engine
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql as psql
import openpyxl
from box import Box
from aiohttp import web, ClientSession, ClientResponseError

import m_telegram
import m_http

from model import Asset, User, UserRequest, UserWallet, Order, OrderState


class _Singleton(type):
    """
    Метакласс, гарантирующий один экземпляр приложения
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Application(metaclass=_Singleton):
    """
    Приложение qTest
    """
    def __init__(self):
        """Инициализация приложения"""
        # настройка логгера
        logging.basicConfig(level=logging.INFO)
        logging.info('запуск приложения')

        # получение настроек приложения
        self.opts = Box.from_yaml(filename=os.path.join(os.path.split(os.getcwd())[0], 'etc/conf.yml'),
                                  encoding='utf-8')

        self._loop = asyncio.get_event_loop()
        self._db = None

        # настройка web-приложения
        self.webapp = web.Application()
        m_http.setup(self, self.webapp)

        # настройка телеграм=бота
        self.tgapp = web.Application()
        m_telegram.setup(self, self.tgapp)

        # синхронизация справочника валют с биржей
        self.mrkt = ClientSession(
            raise_for_status=True,
            base_url=self.opts.crypto.coinpaprika_url)


    def run(self):
        """Запуск приложения в бесконечном цикле"""
        self._loop.run_until_complete(self._db_init())
        self._loop.run_until_complete(self._mrkt_sync())
        self._loop.create_task(self._start_srvs())
        self._loop.run_forever()


    @m_http.post('/market/convert')
    async def convert_coins(self, request):
        """Метод совершения сделки по обмену валют"""
        body = await request.json()

        # генерируем ошибку обмена с некоторой вероятностью
        if random.choice((1, 2, 3, 4)) == 1:
            return web.HTTPConflict()

        return web.json_response(data=dict(order_id=body['order_id']))


    @m_telegram.handler(CommandStart())
    async def command_start_handler(self, message) -> None:
        """ Метод обработки команды /start телеграм-бота"""

        # если нет пользователя, то создаем его
        async with self._db.acquire() as conn:
            result = await conn.execute(
                psql.insert(User)
                .values({"telegram_id": message.from_user.id,
                         "full_name": message.from_user.full_name,
                         "username": message.from_user.username,
                         "raw_request": dict(message.from_user)})
                .on_conflict_do_nothing(index_elements=['telegram_id'])
                .returning(User.id))

            usr_row = await result.fetchone()
            if not usr_row:
                logging.info('Пользователь уже зарегистрирован')
                await message.answer(f"Я помню тебя, {message.from_user.full_name}. "
                                     f"Увы, новой 1000 USDT у меня для тебя нет")
                return

        await message.answer(f"Привет, {message.from_user.full_name}")

        # Начисляем пользователю пробные 1000 USDT
        gen_wlt = (
            sa.select([sa.literal(usr_row.id),
                       sa.cast(1000, sa.types.DECIMAL()),
                       Asset.id])
            .where(Asset.symbol == 'USDT')
            .limit(1)
            .cte('usdt'))

        async with self._db.acquire() as conn:
            result = await conn.execute(
                sa.insert(UserWallet)
                .from_select(['user_id', 'amount', 'asset_id'], gen_wlt)
                .returning(UserWallet.amount))

            wlt_row = await result.fetchone()

        await message.answer(f"Тебе доступно {hbold(wlt_row.amount)} USDT для пробной торговли. "
                             f"Удачи!")


    @m_telegram.handler()
    async def message_handler(self, message: Message) -> None:
        """ Метод обработки сообщений телеграм-бота"""
        text = message.text

        if not text:
            logging.error('Бот работает только с текстом')
            await message.answer('Бот работает только с текстом')
            return

        # запрос отчета
        if text.lower() == 'report':
            await self._process_report(message)
            return

        # запрос сделки
        if order := self._get_order_from_txt(text):
            await self._process_order(order, message)
            return

        # текст в поддержку
        else:
            await self._process_text(message)
            return


    async def _get_user_id(self, telegram_user_id):
        """Получение внутреннего id пользователя по телеграм id"""
        async with self._db.acquire() as conn:
            result = await conn.execute(
                sa.select([User.id])
                .where(User.telegram_id == telegram_user_id))
            row = await result.fetchone()
        return row.id


    @staticmethod
    def _get_order_from_txt(text):
        """Поиск шаблона сделки в тексте и его парсинг"""
        msg_pattern = r'^(?i)(buy|sell) (\d+)([,.]\d+)? ([A-Z, 0-9]*)/([A-Z, 0-9]*)$'
        if match := re.match(msg_pattern, text):
            if match.group(3):
                amount = float(match.group(2) + '.' + match.group(3)[1:])
            else:
                amount = float(match.group(2))
            return dict(type=match.group(1).lower(),
                        amount=amount,
                        base=match.group(4).upper(),
                        quote=match.group(5).upper())


    async def _process_report(self, message: Message):
        """Обработка запроса на получение отчета"""
        user_id = await self._get_user_id(message.from_user.id)

        # получаем актуальные данные кошелька
        sel_wlt = (
            sa.select([Asset.symbol,
                       Asset.name,
                       UserWallet.amount])
            .select_from(UserWallet)
            .join(Asset, Asset.id == UserWallet.asset_id)
            .where(UserWallet.user_id == user_id)
            .order_by(UserWallet.amount.desc()))

        ast_1 = sa.alias(Asset)
        ast_2 = sa.alias(Asset)

        # получаем данные по сделкам за прошедший месяц
        sel_ords = (
            sa.select([OrderState.created,
                       Order.type,
                       Order.amount,
                       ast_1.c.symbol,
                       ast_1.c.name,
                       sa.case([(Order.type == 'buy', sa.cast('spent', sa.types.String()))],
                               else_=sa.cast('get', sa.types.String())).label('act'),
                       OrderState.price,
                       ast_2.c.symbol,
                       ast_2.c.name])
            .select_from(Order)
            .join(OrderState, sa.and_(OrderState.id == Order.last_state_id,
                                      OrderState.name == 'done',
                                      OrderState.created >= sa.func.now() - sa.cast('1 month', sa.types.Interval())))
            .join(ast_1, ast_1.c.id == Order.base_asset_id)
            .join(ast_2, ast_2.c.id == Order.quote_asset_id)
            .where(Order.user_id == user_id)
            .order_by(OrderState.created.desc()))

        async with self._db.acquire() as conn:
            res = await conn.execute(sel_wlt)
            wlt_rows = await res.fetchall()

            res = await conn.execute(sel_ords)
            ords_rows = await res.fetchall()

        # в отдельном процессе выполняем обработку данных и сбор в xlsx
        with ProcessPoolExecutor(max_workers=1) as executor:
            future = executor.submit(self._make_report_xlsx,
                                     [dict(row)for row in wlt_rows],
                                     [dict(row)for row in ords_rows])
            bytes_str = future.result()

        f = BufferedInputFile(bytes_str.getvalue(), filename="report.xlsx")
        await message.answer_document(f)


    @staticmethod
    def _make_report_xlsx(wlt, ords):
        """Генерация xlsx с данными отчета"""
        wb = openpyxl.Workbook()
        # страница кошелька
        # так как wb создается сразу с листом Sheet, то переименовываем его
        wlt_s = wb.get_sheet_by_name('Sheet')
        wlt_s.title = 'wallet'
        if wlt:
            wlt_s.append(tuple(wlt[0].keys()))
            for row in wlt:
                xlsx_row = list()
                for val in row.values():
                    if isinstance(val, (int, float, decimal.Decimal)):
                        xlsx_row.append(float(val))
                    else:
                        xlsx_row.append(str(val))
                wlt_s.append(xlsx_row)

        # страница сделок
        ords_s = wb.create_sheet("done_orders", 1)
        if ords:
            ords_s.append(tuple(ords[0].keys()))
            for row in ords:
                xlsx_row = list()
                for val in row.values():
                    if isinstance(val, (int, float, decimal.Decimal)):
                        xlsx_row.append(float(val))
                    else:
                        xlsx_row.append(str(val))
                ords_s.append(xlsx_row)

        b_str = BytesIO()
        wb.save(b_str)
        return b_str


    async def _process_text(self, message: Message):
        """Обработка входящего сообщения телеграм-бота"""
        user_id = await self._get_user_id(message.from_user.id)

        async with self._db.acquire() as conn:
            await conn.execute(
                sa.insert(UserRequest)
                .values(user_id=user_id,
                        text=message.text))

        await message.answer('Сообщение зарегистрировано. В скором '
                             'времени мы вернемся с ответом')


    async def _get_assets(self, base, quote):
        """Метод получения id валют по их символу symbol"""
        select = (
            sa.select([Asset.coinpaprika_id,
                       Asset.symbol,
                       Asset.id])
            .limit(1)
            .where(Asset.is_active == True)
            .order_by(Asset.rank))

        async with self._db.acquire() as conn:
            result = await conn.execute(
                sa.union(select.where(Asset.symbol == base),
                         select.where(Asset.symbol == quote)))
            return {row['symbol']: dict(coinpaprika_id=row['coinpaprika_id'],
                                        id=row['id']) for row in await result.fetchall()}


    async def _process_order(self, order: dict, message: Message):
        """Обработка запроса на сделку"""
        user_id = await self._get_user_id(message.from_user.id)

        # получаем валюты и убеждаемся, что они есть в справочнике
        assets = await self._get_assets(order['base'], order['quote'])
        if diff := {order['base'], order['quote']}.difference(set(assets)):
            logging.error(f'Валюты не найдены {diff}')
            await message.answer(f'Валюты не найдены {diff}')

        # создаем новый заказ
        order_id = await self._create_new_order(user_id, order, assets)

        # добавляем валюты в кошелек пользователя, если у него их нет
        ins_wlt = (
            psql.insert(UserWallet)
            .values([dict(user_id=user_id,
                          amount=0,
                          asset_id=asset['id']) for asset in assets.values()])
            .on_conflict_do_nothing(index_elements=['user_id', 'asset_id']))

        async with self._db.acquire() as conn:
            await conn.execute(ins_wlt)

        # запрашиваем актуальный курс обмена валюты
        resp = await self.mrkt.get('/v1/price-converter',
                                   params=dict(base_currency_id=assets[order['base']]['coinpaprika_id'],
                                               quote_currency_id=assets[order['quote']]['coinpaprika_id'],
                                               amount=order['amount']))
        conv = await resp.json()
        price = conv['price']

        # определяем возможность выполнения сделки
        def _balance_query(asset_id, amount):
            # запрос проверки остатка баланса пользователя
            return (
                sa.select([UserWallet.id])
                .select_from(UserWallet)
                .join(Asset, Asset.id == UserWallet.asset_id)
                .where(Asset.id == asset_id)
                .where(sa.func.coalesce(UserWallet.amount, 0) >= amount)
                .where(UserWallet.user_id == user_id))

        if order['type'] == 'buy':
            async with self._db.acquire() as conn:
                result = await conn.execute(
                    _balance_query(assets[order['quote']]['id'], price))
                if result.rowcount == 0:
                    logging.error(f'Недостаточно {order["quote"]} для совершении операции')
                    await message.answer(f'Недостаточно {order["quote"]} для совершении операции')
                    return
        else:
            async with self._db.acquire() as conn:
                result = await conn.execute(
                    _balance_query(assets[order['base']]['id'], order['amount']))
                if result.rowcount == 0:
                    logging.error(f'Недостаточно {order["base"]} для совершении операции')
                    await message.answer(f'Недостаточно {order["base"]} для совершении операции')
                    return

        # отправляем запрос на выполнение сделки
        async with ClientSession(raise_for_status=True) as session:
            try:
                await session.post(f'{self.opts.crypto.local_url}/market/convert',
                                   json=dict(order_id=str(order_id),
                                             amount=order['amount'],
                                             base=order['base'],
                                             quote=order['quote']))
            except ClientResponseError:
                logging.error('Обмен не выполнен на стороне биржи')
                await self._set_order_state(order_id=order_id, name='failed')
                await message.answer('Обмен не выполнен на стороне биржи. Повторите попытку')
                return

        logging.info(f'Обмен совершен биржей order_id={str(order_id)}')
        await self._set_order_state(order_id=order_id, name='done', price=price)

        # обновляем балансы кошельков пользователя
        if order['type'] == 'buy':
            base_func = UserWallet.amount + sa.cast(order['amount'], sa.types.DECIMAL())
            quote_func = UserWallet.amount - sa.cast(price, sa.types.DECIMAL())
        else:
            base_func = UserWallet.amount - sa.cast(order['amount'], sa.types.DECIMAL())
            quote_func = UserWallet.amount + sa.cast(price, sa.types.DECIMAL())

        upd_base = (
            sa.update(UserWallet)
            .values(amount=base_func)
            .where(UserWallet.asset_id == assets[order['base']]['id'])
            .where(UserWallet.user_id == user_id)
            .returning(UserWallet.user_id)
            .cte('upd_base'))

        upd_quote = (
            sa.update(UserWallet)
            .values(amount=quote_func)
            .where(UserWallet.asset_id == assets[order['quote']]['id'])
            .where(UserWallet.user_id == upd_base.c.user_id)
            .returning(UserWallet.user_id))

        async with self._db.acquire() as conn:
            await conn.execute(upd_quote)

        sel_bal = (
            sa.select([Asset.symbol,
                       UserWallet.amount])
            .select_from(UserWallet)
            .join(Asset, Asset.id == UserWallet.asset_id)
            .where(UserWallet.user_id == user_id)
            .where(UserWallet.asset_id.in_((assets[order['base']]['id'],
                                            assets[order['quote']]['id']))))

        async with self._db.acquire() as conn, conn.begin():
            result = await conn.execute(sel_bal)
            rows = await result.fetchall()

        await message.answer(f'Обмен успешно совершен. Балансы '
                             f'{rows[0]["symbol"]}: {rows[0]["amount"]}, '
                             f'{rows[1]["symbol"]}: {rows[1]["amount"]}')


    async def _create_new_order(self, user_id, order, assets):
        """Метод создания нового заказа"""
        # создание заказа и статуса new для него выполняем одним запросом
        gen_o = (
            sa.select([sa.literal(user_id),
                       sa.literal(order['type']),
                       sa.cast(order['amount'], sa.types.DECIMAL()),
                       sa.literal(assets[order['base']]['id']),
                       sa.literal(assets[order['quote']]['id']),
                       sa.func.uuid_generate_v4()])
            .cte('gen_o'))

        ins_o = (
            sa.insert(Order)
            .from_select(['user_id', 'type', 'amount', 'base_asset_id',
                          'quote_asset_id', 'last_state_id'],
                         gen_o)
            .returning(Order.id, Order.last_state_id)
            .cte('ins_o'))

        gen_ost = (
            sa.select([ins_o.c.id,
                       ins_o.c.last_state_id,
                       sa.literal('new')])
            .cte('gen_ost'))

        ins_ost = (
            sa.insert(OrderState)
            .from_select(['order_id', 'id', 'name'], gen_ost)
            .returning(OrderState.order_id))

        async with self._db.acquire() as conn:
            result = await conn.execute(ins_ost)
            row = await result.fetchone()

        return row.order_id


    async def _set_order_state(self, order_id, name, price=None):
        """Метод установки состояния заказа"""
        # создание нового состояния и обновление заказа выполняем одним запросом
        ins_ost = (
            sa.insert(OrderState)
            .values(order_id=order_id,
                    name=name,
                    price=price)
            .returning(OrderState.order_id, OrderState.id)
            .cte('ins_ost'))

        upd_o = (
            sa.update(Order)
            .values(last_state_id=ins_ost.c.id)
            .where(Order.id == ins_ost.c.order_id)
            .returning(Order.id))

        async with self._db.acquire() as conn:
            await conn.execute(upd_o)


    async def _start_srvs(self):
        """Запуск серверов"""
        runner = web.AppRunner(self.tgapp)
        await runner.setup()
        site = web.TCPSite(runner, self.opts.telegram.host, self.opts.telegram.port)
        await site.start()
        logging.info('tgapp запущен')

        runner = web.AppRunner(self.webapp)
        await runner.setup()
        site = web.TCPSite(runner, self.opts.http.host, self.opts.http.port)
        await site.start()
        logging.info('webapp запущен')
        logging.info('приложение готово')


    async def _db_init(self):
        """Инициализация БД"""
        self._db = await create_engine(self.opts.postgres.connection)
        logging.info('database запущена')


    async def _mrkt_sync(self):
        """Синхронизация справочника валют"""
        resp = await(await self.mrkt.get('/v1/coins')).json()

        async with self._db.acquire() as conn:
            await conn.execute(
                psql.insert(Asset)
                .values([{"coinpaprika_id": row['id'],
                          "name": row['name'],
                          "symbol": row['symbol'],
                          "type": row['type'],
                          "rank": row['rank'],
                          "is_active": row['is_active']}
                         for row in resp])
                .on_conflict_do_nothing(index_elements=['coinpaprika_id']))

        logging.info('assets синхронизированы')

