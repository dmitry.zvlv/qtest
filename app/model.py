from sqlalchemy import (Column, String, Integer, Boolean, ForeignKey, BIGINT,
                        UniqueConstraint, TIMESTAMP, DECIMAL)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects import postgresql
from sqlalchemy.sql import func

Base = declarative_base()


class Asset(Base):
    __tablename__ = "assets"

    id = Column(postgresql.UUID(as_uuid=True), primary_key=True,
                server_default=func.uuid_generate_v4())
    coinpaprika_id = Column(String, nullable=False, unique=True)
    name = Column(String, nullable=False)
    symbol = Column(String, nullable=False)
    type = Column(String, nullable=False)
    rank = Column(Integer)
    is_active = Column(Boolean)

    created = Column(TIMESTAMP(timezone=True), server_default=func.now())


class User(Base):
    __tablename__ = "users"

    id = Column(postgresql.UUID(as_uuid=True), primary_key=True,
                server_default=func.uuid_generate_v4())
    telegram_id = Column(BIGINT, nullable=False, unique=True)
    full_name = Column(String)
    username = Column(String)
    raw_request = Column(postgresql.JSONB())

    created = Column(TIMESTAMP(timezone=True), server_default=func.now())


class UserRequest(Base):
    __tablename__ = "user_requests"

    id = Column(postgresql.UUID(as_uuid=True), primary_key=True,
                server_default=func.uuid_generate_v4())
    user_id = Column(postgresql.UUID(as_uuid=True),
                     ForeignKey('users.id', name='user_id_fkey'),
                     nullable=False)
    text = Column(String)

    created = Column(TIMESTAMP(timezone=True), server_default=func.now())


class UserWallet(Base):
    __tablename__ = "user_wallets"

    id = Column(postgresql.UUID(as_uuid=True), primary_key=True,
                server_default=func.uuid_generate_v4())
    user_id = Column(postgresql.UUID(as_uuid=True),
                     ForeignKey('users.id', name='user_id_fkey'),
                     nullable=False)
    amount = Column(DECIMAL, nullable=False)
    asset_id = Column(postgresql.UUID(as_uuid=True),
                      ForeignKey('assets.id', name='master_asset_id_fkey'),
                      nullable=False)

    created = Column(TIMESTAMP(timezone=True), server_default=func.now())
    __table_args__ = (
        UniqueConstraint('user_id', 'asset_id', name='user_id_asset_id_unique'),
    )


class Order(Base):
    __tablename__ = "orders"

    id = Column(postgresql.UUID(as_uuid=True), primary_key=True,
                server_default=func.uuid_generate_v4())
    user_id = Column(postgresql.UUID(as_uuid=True),
                     ForeignKey('users.id', name='user_id_fkey'))
    type = Column(String, nullable=False)
    amount = Column(DECIMAL, nullable=False)
    base_asset_id = Column(postgresql.UUID(as_uuid=True),
                           ForeignKey('assets.id', name='master_asset_id_fkey'),
                           nullable=False)
    quote_asset_id = Column(postgresql.UUID(as_uuid=True),
                            ForeignKey('assets.id', name='slave_asset_id_fkey'),
                            nullable=False)
    last_state_id = Column(postgresql.UUID(as_uuid=True))

    created = Column(TIMESTAMP(timezone=True), server_default=func.now())


class OrderState(Base):
    __tablename__ = "order_states"

    id = Column(postgresql.UUID(as_uuid=True), primary_key=True,
                server_default=func.uuid_generate_v4())
    order_id = Column(postgresql.UUID(as_uuid=True),
                      ForeignKey('orders.id', name='order_id_fkey'))
    name = Column(String)
    price = Column(DECIMAL())

    created = Column(TIMESTAMP(timezone=True), server_default=func.now())








