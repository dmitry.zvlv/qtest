"""
Модуль работы с http
Предоставляет возможность использования методов aiohttp в качестве декораторов методов своего класса,
преобразовывает неподдерживаемые json типы данных
"""
from functools import wraps, partial
import logging
from json import dumps, JSONEncoder
from uuid import UUID
from typing import Any
from decimal import Decimal

from aiohttp import web


def get(path: str):
    return __method('GET', path)


def post(path: str):
    return __method('POST', path)


def __method(method, path):
    def wrapper(func):
        @wraps(func)
        async def inner(slf, request: web.Request, *args, **kwargs):
            try:
                return await func(slf, request, *args, **kwargs)
            except web.HTTPException:
                raise
            except Exception:
                logging.exception('Необработанная ошибка')
                raise web.HTTPInternalServerError()

        inner.http = dict(method=method, path=path)
        return inner

    return wrapper


def json_response(data: Any, status: int = 200):
    return web.json_response(data=data, status=status, dumps=partial(dumps, cls=ServiceResponseEncoder))


class ServiceResponseEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            return str(obj)
        elif isinstance(obj, Decimal):
            return float(Decimal)
        return JSONEncoder.default(self, obj)


def setup(app, webapp) -> None:
    routs = list()
    for name in [func for func in dir(app) if callable(getattr(app, func))]:
        func = getattr(app, name)
        if hasattr(func, 'http'):
            route = func.http
            routs.append(web.route(route['method'], route['path'], func))
            logging.info(f"HTTP add route {route['method']} {route['path']}")

    webapp.router.add_routes(routs)

