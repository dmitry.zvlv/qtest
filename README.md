# qTest

qTest - телеграм-бот, позволяющий обменивать криптовалюту на бирже. (Тестовое приложение)

## Installation

Порядок установки на сервере Ubuntu.

Установить PostgreSQL. Создать базу данных test и пользователя test. Установить в БД расширение для работы с uuid.

```bash
sudo apt install postgres
sudo -u postgres psql
create role test login password '<password>';
create database test with owner test;
\c test
create extension if not exists "uuid-ossp";
```

Установить nginx. Натсроить сертификаты ssl (можно с помощью certbot).

```bash
sudo apt install nginx
sudo nano /etc/nginx/sites-enabled/default
```

Прописать в конфигурации вебхук к телеграм-боту.

```
location /webhook {
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_redirect off;
        proxy_buffering off;
        proxy_pass http://127.0.0.1:<port>;
    }
```

Проверить конфигурацию и перезагузить nginx

```bash
sudo nginx -t
sudo service nginx restart
```
Установить Git и скачать файлы проекта. Перейти в папку с проектом. Здесь используется простейшая установка без использования docker.

```bash
sudo apt install git
git clone <project url>
```

Установить alembic и запустить миграцию базы данных. Выполняется из папки проекта /qtest

```bash
sudo apt install alembic
alembic upgrade head
```

Установить pip и установить необходимые python библиотеки. Показан пример без использования окружения environment. Выполняется из папки проекта /qtest

```bash
sudo apt install pip
pip install -r requirements.txt
```

Все готово к запуску проекта

## Использование

Проект запускается напрямую из папки приложения вызовом файла qtest/app/run.py

```bash
python3 run.py
```
